const request = new XMLHttpRequest()

// PRESS ENTER SENDS
var inputText = document.getElementById("inputli");
var inputTextList = document.getElementById("input_list");
var idUsuari = localStorage.getItem('userId');
var usernameLog = localStorage.getItem('userName');

inputText.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        newElement();
    }
});

inputTextList.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        newList();
    }
});

// CARGAR ITEMS DE CADA LISTA
function getItems(list_number) {
    console.log("getItems()");

    document.getElementById("lista_ul").innerHTML = "";

    var endpoint_items = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+list_number+'/todoitems';

    fetch(endpoint_items)
        .then(response => response.json())
        .then(data => renderItems(data));
}

function renderItems(data) {
    var items = data;
    for (i = 0; i < items.length; i++) {
        renderItem(items[i]);
    }
}

function renderItem(item) {
    var ul = document.getElementById("lista_ul");
    var li = document.createElement("li");
    var check = document.createElement("input")
    var inputValue = document.createTextNode(item.nomItem);
    check.id = item.idItem
    check.type = "checkbox"
    check.setAttribute("onClick","updateItem(this.id)")
    if (item.checked){
        check.checked = true;
    }
    var img = document.createElement("img");
    img.id = item.idItem
    img.setAttribute("onClick","deleteItem(this.id)")
    img.setAttribute("src","src/proyecto/media/delete.svg")
    img.setAttribute("onerror","this.onerror=null; this.src='src/proyecto/media/delete.png'")
    img.className = "close";

    li.appendChild(check)
    li.appendChild(img)
    li.appendChild(inputValue);
    ul.appendChild(li)
}

/*CREAR NUEVO ELEMENTO EN LA LISTA*/
function newElement() {
    var lista = document.getElementById("titulo_lista")
    var idLlista = lista.className

    var inputValue = inputText.value;


    if (inputValue === '') {
        alert("You have to put something in th element!");
    } else {
        const url = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+idLlista+'/todoitems';

        const item = {
            idLlista: idLlista,
            nomItem: inputValue,
            position: 1,
            checked: false
        };

        request.open('POST', url)
        request.setRequestHeader('Content-Type','application/json')
        request.send(JSON.stringify(item))

        setTimeout(function(){
            getItems(idLlista)
        }, 500);
    }
    document.getElementById("inputli").value = "";
}

// CHECKEAR ITEM
function updateItem(id_item){
    var lista = document.getElementById("titulo_lista")
    var idLlista = lista.className

    const url = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+idLlista+'/todoitems/'+id_item;

    request.open('PUT', url)
    request.send()
}

// ELIMINAR ITEM
function deleteItem(id_item){
    var lista = document.getElementById("titulo_lista")
    var idLlista = lista.className

    const url = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+idLlista+'/todoitems/'+id_item;

    request.open('DELETE', url)
    request.send()

    setTimeout(function(){
        getItems(idLlista)
    }, 500);
}

// CARGAR INFO DE UNA LISTA
function getOneList(id){
    var endpoint_list = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+id;

    fetch(endpoint_list)
        .then(response => response.json())
        .then(data => renderOneList(data));
}

function renderOneList(list) {
    document.getElementById("header_title").innerHTML = "";

    var t = document.createTextNode(list.nomLlista);

    var h2 = document.createElement("h2");
    h2.id = "titulo_lista"
    h2.className = list.idLlista

    h2.appendChild(t);

    document.getElementById("header_title").appendChild(h2);

    document.getElementById("lista_ul").innerHTML = "";

    getItems(list.idLlista)
}

// CARGAR INFO DE UNA LISTA AL CLICAR
function changeList(id) {
    document.getElementById("lista").style.visibility = "visible";
    document.getElementById("principal").style.display = "none";
    getOneList(id)
}

// CARGAR INFO LISTAS EN MENU IZQ
function getLists() {
    console.log("getLists()");
    var endpoint_list = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists';

    fetch(endpoint_list)
        .then(response => response.json())
        .then(data => renderLists(data));
}

// CARGAR INFO LISTAS EN MENU IZQ
function getPrincipalLists() {
    console.log("getLists()");
    var endpoint_list = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists';

    fetch(endpoint_list)
        .then(response => response.json())
        .then(data => renderPrincipalLists(data));
}

function renderLists(data) {
    document.getElementById("menuizq").innerHTML = "";

    var lists = data;

    console.log(lists.length)

    if (lists.length !== 0){
        document.getElementById("divarrow").style.display = "none";
    }

    for (i = 0; i < lists.length; i++) {
        renderList(lists[i]);
    }
}

function renderList(list) {
    var t = document.createTextNode(list.nomLlista);

    var li = document.createElement("a");
    li.className = "clickList"
    li.id = list.idLlista
    var img = document.createElement("img");
    img.className = "close2";
    img.id = list.idLlista
    img.setAttribute("onClick","deleteList(this.id)")
    img.setAttribute("src","src/proyecto/media/delete.svg")
    img.setAttribute("onerror","this.onerror=null; this.src='src/proyecto/media/delete.png'")

    li.setAttribute("onClick","changeList(this.id)")
    li.appendChild(img)
    li.appendChild(t);

    document.getElementById("menuizq").appendChild(li);
}

function renderPrincipalLists(data) {
    var lists = data;
    document.getElementById("principal").innerHTML = "";

    for (i = 0; i < lists.length; i++) {
        renderPrincipalList(lists[i]);
    }
}

function renderPrincipalList(list) {
    var div = document.createElement("div");
    div.id = list.idLlista
    div.className = "lista_princ"
    div.setAttribute("onClick", "changeList(this.id)")
    var h3 = document.createElement("h3")
    var t = document.createTextNode(list.nomLlista);
    h3.appendChild(t)
    var a = document.createElement("a")
    var t2 = document.createTextNode("Elements in list: "+list.llistaItem.length);
    a.appendChild(t2)

    div.appendChild(h3)
    div.appendChild(a);

    document.getElementById("principal").appendChild(div);
}

// ELIMINAR LLISTA
function deleteList(id){
    const url = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/'+id;

    request.open('DELETE', url)
    request.send()

    setTimeout(function(){
        window.location.reload();
    }, 500);
}


/*CREAR NUEVA LISTA*/
function newList() {
    var inputValue = inputTextList.value;

    if (inputValue === '') {
        alert("The list name can't be empty!");
    } else {
        const url = 'https://taskt-api.herokuapp.com/todousers/'+idUsuari+'/todolists/';

        const llista = {
            nomLlista: inputValue
        };

        request.open('POST', url)
        request.setRequestHeader('Content-Type','application/json')
        request.send(JSON.stringify(llista))

        setTimeout(function(){
            window.location.reload()
        }, 500);
    }
}

/*ABRIR Y CERRAR MENU IZQ*/
let abierto = false;
let menubtn = document.getElementById("bars");
let menuizq = document.getElementById("menuizq");

function openNav() {
    if (abierto === true){
        document.getElementById("menuizq").style.width = "700px";
        abierto = false;
    }else{
        document.getElementById("menuizq").style.width = "0";
        abierto = true;
    }
}

menubtn.onclick = function () {
    abierto = !abierto;
    openNav();
    abierto = !abierto;
}

//POP UP
let modal = document.getElementById("myModal");
let addlistbtn = document.getElementById("addlistbtn");

addlistbtn.onclick = function (){
    modal.style.display = "block";
}

window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
    else if (event.target === log_modal && localStorage.getItem('userId') !== null) {
        log_modal.style.display = "none";
    }
    else if (event.target === logout_modal) {
        logout_modal.style.display = "none";
    }
    else if (event.target !== menuizq && event.target !== menubtn){
        menuizq.style.width = "0"
        abierto = false
    }
}

/*MOVER ELEMENTO DE LA LISTA*/
const lista = document.getElementById("lista_ul")
Sortable.create(lista,{})

$(function () {
    document.getElementById("lista").style.visibility = "hidden";

    if (localStorage.getItem('userId') !== null){
        log_modal.style.display = "none";
        getLists()
        getPrincipalLists()
    }
});

var log_modal = document.getElementById("login_modal")
var logout_modal = document.getElementById("info_modal")
let userloginbtn = document.getElementById("userlogin");
var log_btn = document.getElementById("login_button")
let logoutbtn = document.getElementById("logout_button");

userloginbtn.onclick = function (){
    document.getElementById("user_name").textContent=usernameLog
    logout_modal.style.display = "block";
}

log_btn.onclick = function () {
    log_modal.style.display = "none";
}

logoutbtn.onclick = function (){
    localStorage.clear()
    setTimeout(function(){
        window.location.reload()
    }, 500);
}



var inputUser = document.getElementById("input_user");
var inputPassword = document.getElementById("input_password");

function newUser() {
    var endpoint_items = 'https://taskt-api.herokuapp.com/todousers';

    fetch(endpoint_items)
        .then(response => response.json())
        .then(data => renderUsers(data));
}

function renderUsers(data) {
    var input_user = inputUser.value;
    var users = data;
    var exists = false;
    for (i = 0; i < users.length; i++) {
        if(users[i].nomUsuari === input_user){
            exists = true;
        }
    }

    setTimeout(function(){
        createUser(exists)
    },500)
}

function createUser(exists) {
    var input_user = inputUser.value;
    var input_password = inputPassword.value;
    //var crypted_password = btoa(input_password)

    if (input_password === '' || input_user === '') {
        alert("You have fill all the camps!");
    }
    else if(exists){
        alert("This username already exists, try with another one")
    }
    else {
        alert("User created, try loging in!");
        const url = 'https://taskt-api.herokuapp.com/todousers/';

        const usuari = {
            nomUsuari: input_user,
            password: input_password
        };

        request.open('POST', url)
        request.setRequestHeader('Content-Type','application/json')
        request.send(JSON.stringify(usuari))
    }
}

function logUser() {
    var endpoint_items = 'https://taskt-api.herokuapp.com/todousers';

    fetch(endpoint_items)
        .then(response => response.json())
        .then(data => renderLogUsers(data));
}

function renderLogUsers(data) {
    var input_user = inputUser.value;
    var input_password = inputPassword.value;
    var users = data;
    var log = false;
    var id;
    for (i = 0; i < users.length; i++) {
        if(users[i].nomUsuari === input_user && users[i].password === input_password){
            log = true;
            id = users[i].idUsuari
        }
    }

    setTimeout(function(){
        loginUser(log,id, input_user)
    },500)
}

function loginUser(log, id, username) {
    if (!log) {
        alert("Invalid credentials!");
        setTimeout(function(){
            window.location.reload()
        },500)
    }
    else {
        idUsuari = id
        localStorage.setItem('userId',id)
        localStorage.setItem('userName', username)
        setTimeout(function(){
            window.location.reload()
        },500)
    }
}

var registerbtn = document.getElementById("register_button")
var logbtn = document.getElementById("login_button")

registerbtn.onclick = function () {
    newUser()
}
logbtn.onclick = function (){
    logUser()
    log_modal.style.display = "none";
}